# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 14:40:25 2018

@author: n1260126
"""

from PIL import Image
import sys

import pyocr
import pyocr.builders
import pyautogui as pa
from time import sleep
import openpyxl as px

wb = px.Workbook()
ws = wb.create_sheet('phone1')

#画面準備用スリープ
sleep(5)
#GUIツール設定
tools = pyocr.get_available_tools()
if len(tools) == 0:
    print("No OCR tool found")
    sys.exit(1)
tool = tools[0]
langs = tool.get_available_languages()
lang = langs[0]

y=90
p=62
r=1
s=0
#range()←に集計人数を記入
for i in range(p):
    if p-i<=13 and s==0:
        pa.press('pagedown')
        y=158
        s=1
    pa.screenshot(
        imageFilename="screenshot\screenshot"+str(i)+".png",    # 保存先ファイル名
        region=(140,y,180,40)    # 撮影範囲(x,y,width,height)
        )
    y+=68
    if y>=1042:
        y-=884
        pa.press('pagedown')
    img = Image.open("screenshot\screenshot"+str(i)+".png")
    img = img.resize((int(img.width * 2), int(img.height * 2)))
    txt=tool.image_to_string(
    img,
    lang="eng",
    builder=pyocr.builders.TextBuilder(tesseract_layout=6)
)
    
    ws.cell(row=r, column=1).value=txt
    r+=1
    print(txt)
wb.save(r'C:\Users\n1260126\phone2.xlsx')